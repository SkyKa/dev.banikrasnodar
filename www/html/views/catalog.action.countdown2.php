<?php

	$data_endevent = create_countdown2( '30.11.2017' );

	$str = '
<div class="action-over">
	<div class="countdown" id="js-countdown2" data-endevent="' . ( format_countdown2( $data_endevent ) ) . '" >
		<div class="countdown__item countdown__item--large">
		  <div class="countdown__timer js-countdown-days2 countdown__timer_hour" aria-labelledby="day-countdown2">
				2
		  </div>
		  
		  <div class="countdown__label countdown__label_our" id="day-countdown2">Дня</div>
		</div>
		
		<div class="countdown__item mhs">
		  <div class="countdown__timer js-countdown-hours2" aria-labelledby="hour-countdown">
				14
		  </div>
		  
		  <div class="countdown__label" id="hour-countdown2">Часов</div>
		</div>
		
		<div class="countdown__item mhs">
		  <div class="countdown__timer js-countdown-minutes2" aria-labelledby="minute-countdown">
				35
		  </div>
		  
		  <div class="countdown__label" id="minute-countdown2">Минут</div>
		</div>
		
		<div class="countdown__item mhs">
		  <div class="countdown__timer js-countdown-seconds2" aria-labelledby="second-countdown">
				30
		  </div>
		  <div class="countdown__label" id="second-countdown2">Секунд</div>
		</div>
	</div>
</div>';

	echo $str;

function create_countdown2( $date ) {
	$ex_date = explode( '.', $date );
	$now_time = time( );
	$end_time = mktime( 0, 0, 0, $ex_date[ 1 ], $ex_date[ 0 ], $ex_date[ 2 ] );
	if ( $end_time - $now_time < 1 ) {
		$end_time += 259200;
		return create_countdown( date( 'd.m.Y', $end_time ) );
	}
	return date( 'd.m.Y', $end_time );
}

function format_countdown2( $date ) {
	$ex_date = explode( '.', $date );
	$time = mktime( 0, 0, 0, $ex_date[ 1 ], $ex_date[ 0 ], $ex_date[ 2 ] );
	return date( 'F d, Y', $time );
}