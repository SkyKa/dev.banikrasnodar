<?php

	$data_endevent = create_countdown( '30.11.2017' );

	$str = '
<div class="action-over">
	<div class="countdown" id="js-countdown" data-endevent="' . ( format_countdown( $data_endevent ) ) . '" >
		<div class="countdown__item countdown__item--large">
		  <div class="countdown__timer js-countdown-days countdown__timer_hour" aria-labelledby="day-countdown">
				2
		  </div>
		  
		  <div class="countdown__label countdown__label_our" id="day-countdown">Дня</div>
		</div>
		
		<div class="countdown__item mhs">
		  <div class="countdown__timer js-countdown-hours" aria-labelledby="hour-countdown">
				14
		  </div>
		  
		  <div class="countdown__label" id="hour-countdown">Часов</div>
		</div>
		
		<div class="countdown__item mhs">
		  <div class="countdown__timer js-countdown-minutes" aria-labelledby="minute-countdown">
				35
		  </div>
		  
		  <div class="countdown__label" id="minute-countdown">Минут</div>
		</div>
		
		<div class="countdown__item mhs">
		  <div class="countdown__timer js-countdown-seconds" aria-labelledby="second-countdown">
				30
		  </div>
		  <div class="countdown__label" id="second-countdown">Секунд</div>
		</div>
	</div>
</div>';

	echo $str;

function create_countdown( $date ) {
	$ex_date = explode( '.', $date );
	$now_time = time( );
	$end_time = mktime( 0, 0, 0, $ex_date[ 1 ], $ex_date[ 0 ], $ex_date[ 2 ] );
	if ( $end_time - $now_time < 1 ) {
		$end_time += 259200;
		return create_countdown( date( 'd.m.Y', $end_time ) );
	}
	return date( 'd.m.Y', $end_time );
}

function format_countdown( $date ) {
	$ex_date = explode( '.', $date );
	$time = mktime( 0, 0, 0, $ex_date[ 1 ], $ex_date[ 0 ], $ex_date[ 2 ] );
	return date( 'F d, Y', $time );
}