<?php 
$str='
					<div id="item_images_wrap" class="col-lg-6">
						<div>';
						if ($args -> data['sale'] && $args -> data['new'])
						{
							$str.='<div class="rectangle"><p class="trianglep">NEW</p><div class="triangle"></div></div>
							<div class="circle2"><p>-'.$args -> data['sale'].'%</p></div>';
						}
						else
						{
							if ($args -> data['sale'])
							{
								$str.='<div class="rectangle"></div>
								<div class="circle"><p>-'.$args -> data['sale'].'%</p></div>';
							}
							if ($args -> data['new'])
							{
								$str.='<div class="rectangle" style="background-color:#e9002c;"></div>
								<div class="circle" style="background-color:#e9002c;"><p class="circlenewp">NEW</p></div>';
							}
								
							
						}

$str.='						
								<img class="item_bigpicture" src="'.$args -> data['main_img'].'">
							

						<div class="small_pictures multiple-items">';
						$imgs = $args -> gallery;
						$str.='	<a href="'.$args -> data['main_img'].'">
									<img src="'.$args -> data['main_img'].'">
								</a>';	
						foreach ($imgs as $img) {
						$str.='	<a href="'.$img['img'].'">
									<img src="'.$img['img'].'">
								</a>';	
						}
				
	$str.='						</div>
						</div>
					</div>
					<div class="item_description_wrap col-lg-6">
						<p class="item_description_header">'.$args -> data['title'].'</p>
						<div class="item_main_characteristics col-lg-12">
							<p>Основные характеристики:</p>';
						$chars = $args -> chars;
						foreach ($chars as $char) {
							if ($char['type']=='ГАБАРИТНЫЕ РАЗМЕРЫ')
							$str.='<p>'.$char['title'].', <span>'.$char['ed'].'</span>.......................<span>'.$char['count'].'</span></p>';
						}
	$str.='				</div>
						<div class="item_main_description col-lg-12">
							<p>'.$args -> data['ldescription'].'</p>
						</div>';

	$str.='				<div class="catalog_item_cost col-xs-12">
							<p>Цена</p>';
					$rows=$args -> type;
					foreach ($rows as $row) {
							if ($row['new_price']!==null)
							{
								$str.='<p>'.$row['type'].'&nbsp;&nbsp;&nbsp;<span class="old_price">'.$row['old_price'].' руб.</span>&nbsp;&nbsp;&nbsp;'.$row['new_price'].' руб.</p>';
							}
							else
							{
								$str.='<p>'.$row['type'].'&nbsp;&nbsp;&nbsp;'.$row['old_price'].' руб.</p>';
							}
					}			
	$str.='</div>';	

	$str.='				<div class="col-lg-12 col-xs-12 item_price">
							<div class="col-lg-2 item_price_block">
								<p>Цена</p>
							</div>
							<div class="col-lg-2 col-xs-12 item_main_name">';
						$types = $args -> type;
						foreach ($types as $type) {
							$str.='<p><span class="item_name">'.$type['type'].'</span></p>';
						}
	$str.='					</div>
							<div class="col-lg-6 col-xs-12 item_main_price">';
						foreach ($types as $type) {
							if ($type['new_price'])								
								$str.='<p><span class="old_price">'.$type['old_price'].' руб.</span><span>'.$type['new_price'].' руб.</span></p>';
							else
								$str.='<p><span class="old_price">'.$type['old_price'].' руб.</span><span></span></p>';
						}			
	$str.='					</div>							
						</div>
						<div class="col-xs-12 col-lg-6 header_button">
							<button data-toggle="modal" data-target="#order_form"><span>Заказать</span></button>
						</div>
					</div>
					<!--NAV TABS-->
					<div class="col-lg-12 item_main_nav">
					  <ul class="nav nav-tabs item_nav" role="tablist">
					  	<li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Описание</a></li>
				    	<li><a href="#characteristics" aria-controls="profile" role="tab" data-toggle="tab">Характеристики</a></li>
						<li><a href="#photo" aria-controls="profile" role="tab" data-toggle="tab">Фото</a></li>
					    <li><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
					    <li><a href="#reviews" aria-controls="profile" role="tab" data-toggle="tab">Отзывы<span>(15)</span></a></li>
					   </ul>
					</div>
					<!--TAB PANES-->
					<div role="tabpanel" id="home" class="tab-pane active col-lg-12 col-xs-12 item_tab_description">
						<p class="item_tab_description_header">нпо прогресс - бани с доставкой</p>
						<p class="item_tab_main_description">'.$args -> data['description'].'</p>
						<!--<ul class="technologies_ul padding_none">
							<li>Только экологические чистые материалы</li>
							<li>Каждая баня проходит тщательную проверку на качество</li>
							<li>Температура в бане всегда соответствует стандартам</li>
							<li>Гарантирована противопожарная безопасность</li>
						</ul>-->
					</div>';
	$str.='			<div role="tabpanel" id="reviews" class="tab-pane col-lg-12 col-xs-12 item_tab_description">
		
						<div class="col-xs-12 nopadd reviews_slide_r_side_review">
							<h3>Отзывы клиентов:</h3>';

	$reviews = $args -> reviews;
	foreach ($reviews as $review)
	{
		$true_date = date('d.m.Y',$review [ 'datestamp' ] );
		$str.='				<div>							
								<p style="margin-bottom: 5px;">	
								<img src="/static/img/icons/11_bubble.svg">						
								'.$review['feedback'].'
								</p>
								<div class="video_date">									
										<p>'.$true_date.'</p>
								</div>
								<span style="margin-bottom: 40px;">'.$review['name'].','.$review['place'].' р</span>
							</div>';
	}
	

												
	$str.='					</div>
						<div class="but_rev"><button data-toggle="modal" data-target="#send_feedback">Написать отзыв</button></div>
					</div>';

	$str.='			<div role="tabpanel" id="characteristics" class="tab-pane col-lg-12 col-xs-12 item_sub_characteristics">';

							$mtype='';
							foreach ($chars as $char) {
								if ($mtype==$char['type']) continue;
								else
								{
									$str.='<div class="item_main_characteristics col-lg-12 col-xs-12">';
									$mtype = $char['type'];
									$str.='<p>'.$mtype.'</p>';
									foreach ($chars as $char) {
										if ($mtype==$char['type'])
										{
											$str.='<p>'.$char['title'].', <span>'.$char['ed'].'</span>.......................<span>'.$char['count'].'</span></p>';
										}

									}
									$str.='</div>';
								}															
							}

$str.='				</div>
					<div role="tabpanel" id="photo" class="tab-pane col-lg-12 col-xs-12">
						<p>Галерея</p>
						<div class="item_sub_photo">';
						$str.='<a href="'.$args -> data['main_img'].'">
									<img src="'.$args -> data['main_img'].'">	
								   </a>';
						foreach ($imgs as $img) {
							$str.='<a href="'.$img['img'].'">
									<img src="'.$img['img'].'">	
								   </a>';
						}

$str.='					</div>
					<!--<div class="catalog">	
							<button>Ещё</button>	
						</div>-->
					</div>
					<div role="tabpanel" id="video" class="tab-pane col-lg-12 col-xs-12">	
						<p class="video_header">ВИДЕООБЗОРЫ</p>
						<div class="item_video">';
						$videos = $args -> video;
						foreach ($videos as $video) {
							$true_date = date('d.m.Y',$video [ 'datestamp' ] );
							$str.='
							<div class="video_item">
								<iframe src="'.$video['video'].'"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>'.$true_date.'</p>
								</div>
								<p class="video_title">'.$video['title'].'</p>
							</div>';
						}							
											
$str.='					
					</div>	
					<button>Подписаться на наш youtube канал</button>
					</div>									
					<div class="col-lg-12 see_also">
						<div class="background">
							<div class="col-lg-1 eye">
							<img src="/static/img/icons/2_eye.svg">
						</div>
						<p>смотрите так же:</p>
						</div>						
					</div>';
echo $str;
