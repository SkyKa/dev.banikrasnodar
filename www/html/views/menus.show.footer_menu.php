<ul>
<?php

    $items = $data->getItems($menu->id);

    $uri_orig = $_SERVER['REQUEST_URI'];
    $uris = explode('?', $uri_orig);
    $uri = $uris[0];

    foreach($items as $row)
    {
        if($row['visible'] != 1) continue;

        $href = SF.$row['type_link'];

        if($uri == $href)
        {
            $select = ' class="menu-select"';
        }
        else
        {
            $select = '';
        }

        if($row['type'] == '_label')
        {
            $href = $uri_orig."#";
        }

        $sub_items = $data->getItems($menu->id,$row['id']);
        $cnt = count( $sub_items );
        if ($cnt!=0){
             echo '<li class="dropdown_menu"'.$select.'><a href="'.$href.'" '.$row['attr'].' class="jakor">'.$row['title'].'</a> '; 
        }
        else{
           echo '<li '.$select.'><a  href="'.$href.'" '.$row['attr'].' class="jakor">'.$row['title'].'</a> '; 
        }
        if(count($sub_items) > 0)
        {
            echo " <ul  class='dropdown'> ";
            foreach($sub_items as $sub_row)
            {
                if($sub_row['visible'] != 1) continue;

                $href = SF.$sub_row['type_link'];

                if($uri == $href)
                {
                    $select = ' class="menu-select"';
                }
                else
                {
                    $select = '';
                }             

                if($sub_row['type'] == '_label')
                {
                    $href = $uri_orig."#";
                }

                echo '<li'.$select.'><a href="'.$href.'" '.$sub_row['attr'].'>'.$sub_row['title'].'</a></li> ';
            }
            echo " </ul> ";
        }

        echo '</li> ';
    }
?>
</ul>
