
<a href="/">Главная</a> /
<?php
      $table = new Table( 'pages' );
      $id = $table->select( 'SELECT * FROM `pages` WHERE alias=:alias LIMIT 1', array( 'alias' => $alias ) );
      if( count( $id ) > 0 )
      {
          if( $id[ 0 ][ 'parent_id' ] != 0 ) {
              echo show_Breadcrumbs( $id[ 0 ][ 'parent_id' ], '' );
          }
      }

      $str='';
      if ($params[0]) 
      {
        $str="<a class='a-bread' style='color:#3C3D3F' href='/" . $id[ 0 ][ 'alias' ] . "/'>" . $id[0]['title'] . "</a> / " . $str;
        echo ($str);
        $rows = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:id LIMIT 1', array( 'id' => $params[ 0 ] ) ); 
        $row = end( $rows );
        echo ($row['title']);
      }
      else
      {
        echo ($id[0]['title']);

      }

?>

<?php

function show_Breadcrumbs( $id, $str )
{
     $table = new Table( 'pages' );
     $rows = $table->select( 'SELECT * FROM `pages` WHERE id=:id LIMIT 1', array( 'id' => $id ) );
     if( count( $rows ) > 0 )
     {
          $str = "<a class='a-bread' style='color:#3C3D3F' href='/" . $rows[ 0 ][ 'alias' ] . "/'>" . $rows[ 0 ][ 'title' ] . "</a> / " . $str;
          if( $rows[ 0 ][ 'parent_id' ] != 0 )
          {
              $str = show_Breadcrumbs( $rows[ 0 ][ 'parent_id' ], $str );
          }
     }
   return $str;
}

?>

