<?php

	$table = new Table('catalog_section');
    $rows = $table -> select("SELECT * FROM questions ORDER BY `position` ");
    $i = 1;
    foreach ($rows as $row ) {
    	echo '
    	<div class="questions_item">
			<div role="tab" id="heading'.$i.'">
						<h4>
							<img src="/static/img/icons/16_arrow_down.svg" class="arrow_down">
							<img src="/static/img/icons/17_arrow_up.svg" class="arrow_up">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$i.'" aria-expanded="false" aria-controls="collapse'.$i.'">
								'.$row['question'].'
							</a>
						</h4>
					</div>
					<div id="collapse'.$i.'" class="collapse questions_item_text" role="tabpanel" aria-labelledby="heading'.$i.'">
						<p>'.$row['answer'].'</p>
					</div>
			</div>
		</div> 
	 ';
	 $i+=1;
    }
