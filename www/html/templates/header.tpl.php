<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php $title = $registry->title; if($title) echo $title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php out('description')?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="<?php out('keywords')?>" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>

	<link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/forms_js.css' )?>">
    <link rel="shortcut icon" href="/favicon.ico" />

    <link rel="stylesheet" href="/static/fonts/font-awesome/css/font-awesome.min.css">
	<link href="/static/css/style.css" rel="stylesheet">
	<link href="/static/css/media.css" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link href="/static/css/bootstrap.min.css" rel="stylesheet">
	<!-- Optional theme -->
	<link href="/static/css/bootstrap-theme.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/static/css/lightgallery.min.css"/>
	<link rel="stylesheet" type="text/css" href="/static/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="/static/slick/slick-theme.css"/>
	<link href="/static/css/forms_js.css" rel="stylesheet">

</head>
<body>
	<header>
		<section>
			<div class="container-fluid">
				<div class="row row_header">
					<div class="container">
						<div class="col-xs-12 col-sm-3 header_logo">
							<a href="/"><img src="/static/img/footer_logo.png"></a>
							<p>НПО прогресс</p>
						</div>
						<div class="col-xs-12 col-sm-3 header_phone">
							<a href='tel:<?php echo Utils :: phone_number( val( 'banner.show.phone' ) )?>'><?php mod('banner.show.phone') ?></a>
						</div>
						<div class="col-xs-12 col-sm-3 header_button">
							<button data-toggle="modal" data-target="#feedback_form">Оставить заявку</button>
						</div>
						<div class="col-xs-12 col-sm-3 header_mail header_search">
							<a href='mailto:<?php mod('banner.show.e-mail') ?>'><?php mod('banner.show.e-mail') ?></a>
							<div class="search">
								<img src="/static/img/icons/1_loupe.svg">
								<input type="text" name="header_search">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="header_section"> 
			<div class="container-fluid">
				<div class="row row_header_nav">
					<div class="container header_nav">
						<?php mod("menus.show.header_menu"); ?>
					</div>
				</div>
			</div>
		</section>
	</header>