<?php include "html/templates/header.tpl.php" ?>


	<section>
		<div class="container-fluid">
			<div class="row row_block_1">
				<div class="first_slider">
					<?php mod('catalog.action.main_slider')?>
				</div>
			</div>
		</div>
	</section>

	<section id="about">
		<div class="container-fluid">
			<div class="row">
				<div class="container infoblock_1">
					<h1>нпо прогресс — бани с доставкой</h1>
					<p>ООО «НПО Прогресс» основан в 1998 году и оснащен современным технологическим оборудованием, приборами для входного контроля исходных материалов, операционного контроля изготовления литейных форм, термической и механической обработки, приемки готовой продукции. Собственные производственные помещения располагаются в г. Краснодар, г. Челябинск, г. Могилев РБ. Доставка готовой продукции производится по всей России и республике Беларусь на собственном транспорте ООО НПО «Прогресс».</p>
					<img src="/static/img/infoblock1.png">
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-fluid">
			<div class="row row_infoblock_3">
				<h2>Наши партнёры</h2>
				<div class="infoblock_3_multiple">
					<?php mod('catalog.action.compan')?>;
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="container infoblock_4">
					<h2>Приемущества работы с нами</h2>
					<div class="col-xs-12 infoblock_4_1_wrap">
						<div class="col-xs-12 col-sm-3 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/4_saw.svg">
							</div>
							<h3>Опыт работы более 19 лет (с 1998 года)</h3>
						</div>
						<div class="col-xs-12 col-sm-3 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/5_calendar.svg">
							</div>
							<h3>Изготовление бани от 10 дней</h3>
						</div>
						<div class="col-xs-12 col-sm-3 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/6_timer.svg">
							</div>
							<h3>Установка бани от 1 часа</h3>
						</div>
						<div class="col-xs-12 col-sm-3 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/7_fire.svg">
							</div>
							<h3>Противопожарная безопасность подтверждена сертификатами</h3>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="col-xs-12 col-sm-offset-2 col-sm-2 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/8_wallet.svg">
							</div>
							<h3>Предоплата всего 25%</h3>
						</div>
						<div class="col-xs-12 col-sm-offset-1 col-sm-2 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/9_percent.svg">
							</div>
							<h3>Возможность рассрочки или кредита</h3>
						</div>
						<div class="col-xs-12 col-sm-offset-1 col-sm-2 infoblock_4_item">
							<div class="infoblock_4_image">
								<img src="/static/img/icons/10_medal.svg">
							</div>
							<h3>Гарантия на баню 5 лет</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="not_over">
		<div class="container-fluid">
			<div class="row row_1_form not_over">
				<img src="/static/img/row_1_form_back.jpg">
				<div class="container wrap_1_form">
					<div class="col-xs-12 col-lg-8 wrap_1_form">
						<h2>Бесплатная доставка</h2>
						<h2>При 100% оплате<sup>*</sup></h2>
							<div class="form-group">
							<?php 
				      			mod('catalog.action.forms',array('alias'=>'hochu_besplatnuyu_dostavku'));
					      	?>
								<!--<input type="text" name="email" placeholder="E-mail">
								<input type="text" name="phone" placeholder="Телефон">
								<p><input type="checkbox" id="privacy_offer"><label for="privacy_offer">Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a href="#">политикой конфиденциальности</a></label></p>
								<button>Хочу бесплатную доставку</button>-->
							</div>
					</div>
					<div class="background">
						<!--<img class="wave_girl" src="/static/img/wrap_1_form_vawe.png">-->
						<canvas class="background__canvas" data-image="/static/img/wrap_1_form_vawe.jpg" data-mask="/static/img/wrap_1_form_vawe2.jpg"></canvas>
					</div>				
				</div>
			</div>
		</div>
	</section>
	
	<section  id="catalog">
			<div class="catalogmain">
				<?php mod('catalog.action.catalog_list');?>				
				<a href="/catalog/" class="another">Ещё</a>
			</div>

	</section>
	
				

	<section class="section_reviews" id="rew">
		<div class="container-fluid">
			<div class="row">
				<div class="container reviews">
					<h2>Отзывы о готовых заказах</h2>
					<?php mod('catalog.action.reviews'); ?>
					<div class="col-xs-6 review_arrows">
						<div class="review_slide_left">
							<img src="/static/img/icons/12_arrow_left.svg">
						</div>
					</div>
					<div class="col-xs-6 review_arrows">
						<div class="review_slide_right">
							<img src="/static/img/icons/13_arrow_right.svg">
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section class="section_infoblock_5">
		<div class="container-fluid">
			<div class="row row_infoblock_5">
				
				<div class="container infoblock_5">
					<h2>Для оформления заявки - звоните по номеру</h2>
					<div class="infoblock_5_wrap">
						<div class="infoblock_5_mobile">
							<img src="/static/img/icons/14_phone.svg">
						</div>
						<div class="infoblock_5_number">
							<a href='tel:<?php echo Utils :: phone_number( val( 'banner.show.phone' ) )?>'><?php mod('banner.show.phone') ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="container infoblock_6">
					<h2>Наши бани в разрезе</h2>
					<div class="col-xs-12 col-xs-6">
						<img src="/static/img/razrez_1.png">
					</div>
					<div class="col-xs-12 col-xs-6">
						<img src="/static/img/razrez_2.png">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section_questions" id="qa">
		<div class="container-fluid">
			<div class="row row_questions">
				<img src="/static/img/row_questions_back.png">
				<div class="container questions">
					<div class="col-xs-12 col-md-6 nopadd questions_half_block">
						<h2><img src="/static/img/icons/15_quest.svg">Часто задаваемые вопросы</h2>
						<div class="questions_block">
							<div id="accordion" role="tablist" aria-multiselectable="true">
								<?php mod('catalog.action.qa');?>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section_form_2" id="sale">
		<div class="container-fluid">
			<div class="row row_form_2">
				<img src="/static/img/form_2_back.jpg">
				<div class="container form_2">
					<h2>Получить скидку на 15% на баню</h2>
					<h3>при 100% предоплате</h3>
					<div class="form_2_timer col-xs-12 col-sm-offset-3 col-sm-6">
									<h2>До повышения цен осталось:</h2>
									<?php mod('catalog.action.countdown2')?>;	
					</div>
					<div class="col-xs-12 nopadd form_2_form">
							<div class="form-group">
								<?php 
				      				mod('catalog.action.forms',array('alias'=>'poluchit_skidku'));
					      		?>
								<!--<div class="col-xs-12 col-sm-3">
									<input type="text" name="name_2" placeholder="Имя">
								</div>
								<div class="col-xs-12 col-sm-3">
									<input type="text" name="tel_2" placeholder="Телефон">
								</div>
								<div class="col-xs-12 col-sm-3">
									<input type="text" name="mail_2" placeholder="E-mail">
								</div>
								<div class="col-xs-12 col-sm-3">
									<button>Получить скидку</button>
								</div>
								<p><input type="checkbox" id="privacy_offer_2"><label for="privacy_offer_2">Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a href="#">политикой конфиденциальности.</a></label></p>
							</div>-->
					</div>		
				</div>
			</div>
		</div>
	</section>

	<section id="contact">
		<div class="container-fluid"> 
			<div class="row row_contacts">
				<div class="container contacts">
					<h2>Контакты</h2>
					<div class="col-xs-12 col-sm-3 contacts_production">
						<h3>Производство</h3>
						<p><?php mod('banner.show.contacts-production');?></p>
					</div>
					<div class="col-xs-12 col-sm-3 contacts_sells">
						<h3>Отдел продаж</h3>						
						<p><?php mod('banner.show.contacts-department');?></p>
					</div>
					<div class="col-xs-12 col-sm-3 contacts_numeil">
						<a class="contacts_number" href='tel:<?php echo Utils :: phone_number( val( 'banner.show.phone' ) )?>'><?php mod('banner.show.phone') ?></a>
						<a class="contacts_mail" href='mailto:<?php mod('banner.show.e-mail') ?>'><?php mod('banner.show.e-mail') ?></a>
					</div>
					<div class="col-xs-12 col-sm-3 contacts_socials">
						<ul>
							<li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-fluid">
			<div class="row row_map">
				<div id="map" data-lat="45.2656739" data-lon="39.7832238" data-zoom="16"></div>
			</div>
		</div>
	</section>


<?php include "html/templates/footer.tpl.php" ?>