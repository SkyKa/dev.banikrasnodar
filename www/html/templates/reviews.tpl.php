<?php include "html/templates/header.tpl.php" ?>


<section class="breadcrumbs_section">
		<div class="container-fluid breadcrumbs">
			<div class="row">
				<div class="container">
					<div class="col-xs-12">
						<?php mod('catalog.action.breadcrumbs');?>
					</div>					
				</div>
			</div>	
		</div>
</section>

<section class="section_reviews">
	<div class="container-fluid">
		<div class="row">
			<div class="container reviews2">
				<h2>Отзывы о готовых заказах</h2>
				<div class="reviews_slide">
					<div class="col-xs-12 nopadd">1
						<div class="col-xs-12 col-md-7 reviews_slide_l_side">
								<img src="/static/img/review_big_image.jpg">							
							<div class="reviews_slide_l_side_choice">
								<img src="/static/img/reviews_slide_l_side_choice_1.jpg">
								<img src="/static/img/reviews_slide_l_side_choice_2.png">
								<img src="/static/img/reviews_slide_l_side_choice_3.png">
							</div>
						</div>
						<div class="col-xs-12 col-md-5 reviews_slide_r_side">
							<div class="col-xs-12 nopadd reviews_slide_r_side_description">
								<h3>Баня-бочка</h3>
								<div class="col-xs-6 nopadd reviews_slide_r_side_description_l">
									<p>Срок изготовления</p>
									<p>Толщина стенки, мм</p>
									<p>Диаметр бочки, мм</p>
									<p>Длина корпуса снаружи, м</p>
								</div>
								<div class="col-xs-6 reviews_slide_r_side_description_r">
									<p>10 дней</p>
									<p>23</p>
									<p>234</p>
									<p>2</p>
								</div>									
							</div>
							<div class="col-xs-12 nopadd reviews_slide_r_side_review">
								<h3><img src="/static/img/icons/11_bubble.svg">Отзыв клиента</h3>
								<p>Кутана неустойчива. Парафинирование мгновенно. Осушение ненаблюдаемо трансформирует фраджипэн. Горизонт отталкивает карбонат кальция, что дает возможность использования данной методики.</p>								
								<span>Евгений Савельев, г. Краснодар</span>
								<div class="rew_date">									
										<p>28.10.17</p>
								</div>
								<button>Заказать такую же</button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 nopadd">2
						<div class="col-xs-12 col-md-7 reviews_slide_l_side">
							<img src="/static/img/review_big_image.jpg">
							<div class="reviews_slide_l_side_choice">
								<img src="/static/img/reviews_slide_l_side_choice_1.jpg">
								<img src="/static/img/reviews_slide_l_side_choice_2.png">
								<img src="/static/img/reviews_slide_l_side_choice_3.png">
							</div>
						</div>
						<div class="col-xs-12 col-md-5 reviews_slide_r_side">
							<div class="col-xs-12 nopadd reviews_slide_r_side_description">
								<h3>Баня-бочка</h3>
								<div class="col-xs-6 nopadd reviews_slide_r_side_description_l">
									<p>Срок изготовления</p>
									<p>Толщина стенки, мм</p>
									<p>Диаметр бочки, мм</p>
									<p>Длина корпуса снаружи, м</p>
								</div>
								<div class="col-xs-6 reviews_slide_r_side_description_r">
									<p>10 дней</p>
									<p>23</p>
									<p>234</p>
									<p>2</p>
								</div>									
							</div>
							<div class="col-xs-12 nopadd reviews_slide_r_side_review">
								<h3><img src="/static/img/icons/11_bubble.svg">Отзыв клиента</h3>
								<p>Кутана неустойчива. Парафинирование мгновенно. Осушение ненаблюдаемо трансформирует фраджипэн. Горизонт отталкивает карбонат кальция, что дает возможность использования данной методики.</p>
								<span>Евгений Савельев, г. Краснодар</span>								
								<button>Заказать такую же</button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 nopadd">3
						<div class="col-xs-12 col-md-7 reviews_slide_l_side">
							<img src="/static/img/review_big_image.jpg">
							<div class="reviews_slide_l_side_choice">
								<img src="/static/img/reviews_slide_l_side_choice_1.jpg">
								<img src="/static/img/reviews_slide_l_side_choice_2.png">
								<img src="/static/img/reviews_slide_l_side_choice_3.png">
							</div>
						</div>
						<div class="col-xs-12 col-md-5 reviews_slide_r_side">
							<div class="col-xs-12 nopadd reviews_slide_r_side_description">
								<h3>Баня-бочка</h3>
								<div class="col-xs-6 nopadd reviews_slide_r_side_description_l">
									<p>Срок изготовления</p>
									<p>Толщина стенки, мм</p>
									<p>Диаметр бочки, мм</p>
									<p>Длина корпуса снаружи, м</p>
								</div>
								<div class="col-xs-6 reviews_slide_r_side_description_r">
									<p>10 дней</p>
									<p>23</p>
									<p>234</p>
									<p>2</p>
								</div>									
							</div>
							<div class="col-xs-12 nopadd reviews_slide_r_side_review">
								<h3><img src="/static/img/icons/11_bubble.svg">Отзыв клиента</h3>
								<p>Кутана неустойчива. Парафинирование мгновенно. Осушение ненаблюдаемо трансформирует фраджипэн. Горизонт отталкивает карбонат кальция, что дает возможность использования данной методики.</p>
								<span>Евгений Савельев, г. Краснодар</span>
								<button>Заказать такую же</button>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 review_arrows">
					<div class="review_slide_left">
						<img src="/static/img/icons/12_arrow_left.svg">
					</div>
				</div>
				<div class="col-xs-6 review_arrows">
					<div class="review_slide_right">
						<img src="/static/img/icons/13_arrow_right.svg">
					</div>
				</div>					
			</div>
		</div>
	</div>
</section>


<?php include "html/templates/footer.tpl.php" ?>
