<?php include "html/templates/header.tpl.php" ?>
<section class="breadcrumbs_section">
		<div class="container-fluid breadcrumbs">
			<div class="row">
				<div class="container">
					<div class="col-xs-12">
						<?php mod('catalog.action.breadcrumbs');?>
					</div>					
				</div>
			</div>	
		</div>
</section>
<section class="catalog_item_section">
		<div class="container-fluid">
			<div class="row">
				<div class="container">
					<div id="item_images_wrap" class="col-lg-6">
						<div>
							<!--<div class="rectangle"></div>
							<div class="circle"><p>-10%</p></div>-->
							<a href="/static/img/catalog_item_1.png">
								<img class="item_bigpicture" src="/static/img/catalog_item_1.png">
							</a>
							<div class="small_pictures multiple-items">
									<a href="/static/img/reviews_slide_l_side_choice_3.png">
										<img src="/static/img/reviews_slide_l_side_choice_3.png">
									</a>
									<a href="/static/img/reviews_slide_l_side_choice_1.jpg">
										<img src="/static/img/reviews_slide_l_side_choice_1.jpg">
									</a>
									<a href="/static/img/catalog_item_1.png">
										<img src="/static/img/catalog_item_1.png">
									</a>		
									<a href="/static/img/catalog_item_5.jpg">
										<img src="/static/img/catalog_item_5.jpg">
									</a>							
							</div>
						</div>

					</div>
					<div class="item_description_wrap col-lg-6">
						<p class="item_description_header">Баня-бочка «Трио»</p>
						<div class="item_main_characteristics col-lg-12">
							<p>Основные характеристики:</p>
							<p>Толщина стенки, <span>мм</span>.......................<span>45</span></p>
							<p>Диаметр бочки, <span>мм</span>.........................<span>45</span></p>
							<p>Длина корпуса снаружи, <span>м</span>.............<span>45</span></p>
						</div>
						<div class="item_main_description col-lg-12">
							<p>ООО «НПО Прогресс» основан в 1998 году и оснащен современным 
							технологическим оборудованием, приборами входного контроль
							исходных материалов, операционного контроля изготовления путь
							литейных форм, термической и механической обработки. Прогресс 
							основан в 1998 году и оснащен современным технологическим 
							оборудованием, приборами входного контроль исходных.</p>
						</div>
						<div class="col-lg-12 item_price">
							<div class="col-lg-2 item_price_block">
								<p>Цена</p>
							</div>
							<div class="col-lg-2 item_main_name">
								<p><span class="item_name">Сосна</span></p>
								<p><span class="item_name">Кедр</span></p>
							</div>
							<div class="col-lg-6 item_main_price">
								<p><span class="old_price">146 000 руб.</span><span>115 000 руб.</span></p>
								<p><span class="old_price">188 000 руб.</span><span></span></p>
							</div>							
						</div>
						<div class="col-xs-12 col-lg-6 header_button">
							<button data-toggle="modal" data-target=".modal"><span>Заказать</span></button>
						</div>
					</div>
					<!--NAV TABS-->
					<div class="col-lg-12 item_main_nav">
					  <ul class="nav nav-tabs item_nav" role="tablist">
					  	<li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Описание</a></li>
				    	<li><a href="#characteristics" aria-controls="profile" role="tab" data-toggle="tab">Характеристики</a></li>
						<li><a href="#photo" aria-controls="profile" role="tab" data-toggle="tab">Фото</a></li>
					    <li><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
					    <li><a href="#reviews" aria-controls="profile" role="tab" data-toggle="tab">Отзывы<span>(15)</span></a></li>
					   </ul>
					</div>
					<!--TAB PANES-->
					<div role="tabpanel" id="home" class="tab-pane active col-lg-12 col-xs-12 item_tab_description">
						<p class="item_tab_description_header">нпо прогресс - бани с доставкой</p>
						<p class="item_tab_main_description">ООО «НПО Прогресс» основан в 1998 году и оснащен современным технологическим оборудованием, приборами для входного контроля
						исходных материалов, операционного контроля изготовления литейных форм, термической и механической обработки, приемки готовой
						продукции. Собственные производственные помещения располагаются в г. Краснодар, г. Челябинск, г. Могилев РБ. Доставка готовой
						продукции производится по всей России и республике Беларусь на собственном транспорте ООО НПО «Прогресс».</p>
						<p class="technologies">Технологии производства бань:</p>
						<ul class="technologies_ul padding_none">
							<li>Только экологические чистые материалы</li>
							<li>Каждая баня проходит тщательную проверку на качество</li>
							<li>Температура в бане всегда соответствует стандартам</li>
							<li>Гарантирована противопожарная безопасность</li>
						</ul>
					</div>
					<div role="tabpanel" id="characteristics" class="tab-pane col-lg-12 col-xs-12 item_sub_characteristics">	
						<div class="item_main_characteristics col-lg-12 col-xs-12">
							<p>Габаритные размеры:</p>
							<p>Толщина стенки, <span>мм</span>.......................<span>45</span></p>
							<p>Диаметр бочки, <span>мм</span>.........................<span>45</span></p>
							<p>Длина корпуса снаружи, <span>м</span>.............<span>45</span></p>
						</div>
						<div class="item_main_characteristics col-lg-12 col-xs-12">
							<p>Качественные особенности</p>
							<p>Долговечность..................................<span>45</span></p>
							<p>Впитываемость................................<span>235</span></p>
							<p>Уровень шума...................................<span>3,0</span></p>
						</div>
					</div>
					<div role="tabpanel" id="photo" class="tab-pane col-lg-12 col-xs-12">
						<p>Галерея</p>
						<div class="item_sub_photo">
								<a href="/static/img/catalog_item_1.png">
									<img src="/static/img/catalog_item_1.png">	
								</a>
								<a href="/static/img/catalog_item_1.png">
									<img src="/static/img/catalog_item_1.png">	
								</a>
								<a href="/static/img/reviews_slide_l_side_choice_3.png">
									<img src="/static/img/reviews_slide_l_side_choice_3.png">
								</a>
								<a href="/static/img/reviews_slide_l_side_choice_1.jpg">
									<img src="/static/img/reviews_slide_l_side_choice_1.jpg">
								</a>
								<a href="/static/img/catalog_item_1.png">
									<img src="/static/img/catalog_item_1.png">
								</a>		
								<a href="/static/img/catalog_item_5.jpg">
									<img src="/static/img/catalog_item_5.jpg">
								</a>	
						</div>
							<div class="catalog">	
							<button>Ещё</button>	
						</div>
					</div>
					<div role="tabpanel" id="video" class="tab-pane col-lg-12 col-xs-12">	
						<p class="video_header">ВИДЕООБЗОРЫ</p>
						<div class="item_video">
							<div class="video_item">
								<iframe src="https://www.youtube.com/embed/RX40E5CUfP0"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>17 сентября, 2017</p>
								</div>
								<p class="video_title">Как происходит монтаж бани.<br>Особенности и сикреты</p>
							</div>
							<div class="video_item">
								<iframe src="https://www.youtube.com/embed/RX40E5CUfP0"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>17 сентября, 2017</p>
								</div>
								<p class="video_title">Как происходит монтаж бани.<br>Особенности и сикреты</p>
							</div>
							<div class="video_item">
								<iframe src="https://www.youtube.com/embed/RX40E5CUfP0"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>17 сентября, 2017</p>
								</div>
								<p class="video_title">Как происходит монтаж бани.<br>Особенности и сикреты</p>
							</div>
							<div class="video_item">
								<iframe src="https://www.youtube.com/embed/RX40E5CUfP0"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>17 сентября, 2017</p>
								</div>
								<p class="video_title">Как происходит монтаж бани.<br>Особенности и сикреты</p>
							</div>
							<div class="video_item">
								<iframe src="https://www.youtube.com/embed/RX40E5CUfP0"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>17 сентября, 2017</p>
								</div>
								<p class="video_title">Как происходит монтаж бани.<br>Особенности и сикреты</p>
							</div>
							<div class="video_item">
								<iframe src="https://www.youtube.com/embed/RX40E5CUfP0"  width="350" height="250"  frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
								<div class="video_date">
									<p>17 сентября, 2017</p>
								</div>
								<p class="video_title">Как происходит монтаж бани.<br>Особенности и сикреты</p>
							</div>
						</div>		
						<button>Подписаться на наш youtube канал</button>					
					</div>					
					<div class="col-lg-12 see_also">
						<div class="background">
							<div class="col-lg-1 eye">
							<img src="/static/img/icons/2_eye.svg">
						</div>
						<p>смотрите так же:</p>
						</div>						
					</div>				
				</div>
			</div>
		</div>
		
</section>

	<section class="see_also_items">
		<div class="container-fluid">
			<div class="row">
				<div class="container catalog">
					<div class="catalog_item col-xs-12 col-md-4">
						<img src="/static/img/catalog_item_1.png">
						<h3>Баня-бочка</h3>
						<p>Толщина стенки, мм...........45</p>
						<p>Диаметр бочки, мм...........235</p>
						<p>Длина корпуса снаружи, мм...........3,0</p>
						<div class="catalog_item_cost">
							<p>Сосна&nbsp;&nbsp;&nbsp;146 000 руб.</p>
							<p>Кедр&nbsp;&nbsp;&nbsp;188 000 руб.</p>							
						</div>
						<button>Подробнее</button>
					</div>
					<div class="catalog_item col-xs-12 col-md-4">
						<div class="new_rec"></div>
						<div class="new_cir"><p>NEW</p></div>
						<img src="/static/img/catalog_item_2.png">
						<h3>Баня-бочка</h3>
						<p>Толщина стенки, мм...........45</p>
						<p>Диаметр бочки, мм...........235</p>
						<p>Длина корпуса снаружи, мм...........3,0</p>
						<div class="catalog_item_cost">
							<p>Сосна&nbsp;&nbsp;&nbsp;<span>146 000 руб.</span>&nbsp;&nbsp;&nbsp;146 000 руб.</p>
							<p>Кедр&nbsp;&nbsp;&nbsp;188 000 руб.</p>							
						</div>
						<button>Подробнее</button>
					</div>
					<div class="catalog_item col-xs-12 col-md-4">
						<img src="/static/img/catalog_item_3.jpg">
						<h3>Баня-бочка</h3>
						<p>Толщина стенки, мм...........45</p>
						<p>Диаметр бочки, мм...........235</p>
						<p>Длина корпуса снаружи, мм...........3,0</p>
						<div class="catalog_item_cost">
							<p>Сосна&nbsp;&nbsp;&nbsp;146 000 руб.</p>
							<p>Кедр&nbsp;&nbsp;&nbsp;188 000 руб.</p>							
						</div>
						<button>Подробнее</button>
					</div>
					<button>Ещё</button>					
				</div>
			</div>
		</div>
	</section>

<?php include "html/templates/footer.tpl.php" ?>