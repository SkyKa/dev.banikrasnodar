<?php

// date_default_timezone_set( 'Europe/Moscow' );
date_default_timezone_set( 'Asia/Yekaterinburg' );

// Site name
$site_name = "dev.banikrasnodar";


// MySQL
$db_user = "root";
$db_user_pass = "";
$db_host = "localhost";
$db_name = "dev.banikrasnodar";


if ( isset( $_SERVER[ 'HTTP_HOST' ] ) && $_SERVER[ 'HTTP_HOST' ] ) {
	if ( !defined( 'CLI_MODE' ) ) define( 'CLI_MODE', FALSE );
	$base = 'http://' . $_SERVER['HTTP_HOST'] . '/';
}
else {
	if ( !defined( 'CLI_MODE' ) ) define( 'CLI_MODE', TRUE );
	$base = 'http://' . $db_name . '/';
}


// ACQUIRING
$pay_conf = array(
			'username' => 'test',
			'password' => 'test',
			'siteurl' => $base,
			'return_url' => '/return-url.html',
			'fail_url' => '/fail-url.html',

			'session_timeout' => 7200,
			'class' => 'Alfabank_rest'
);

?>