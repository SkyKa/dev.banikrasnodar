$(document).ready(function(){

	$('.first_slider').slick({
		autoplay: false,
		arrows: true,
		dots: true
	});

	$('.infoblock_3_multiple').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 3,
		dots: true
	});

	$('.reviews_slide').slick({
	  dots: false,
	  prevArrow: $('.review_slide_left'),
	  nextArrow: $('.review_slide_right'),
	  infinite: true,
	  speed: 500,
	  cssEase: 'linear'
	});

	$('body').on('click', '.questions_item h4 a', function(){
		check = $(this).attr('class');
		if (check) {
			$(this).closest('h4').find('.arrow_down').css('display', 'none');
			$(this).closest('h4').find('.arrow_up').css('display', 'block');
		}
		else {
			$(this).closest('h4').find('.arrow_down').css('display', 'block');
			$(this).closest('h4').find('.arrow_up').css('display', 'none');
		}
	});
	
});