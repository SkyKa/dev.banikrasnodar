(function($, undefined){

	"use strict";

	$( window ).on( 'load', function( ) {
		var endevent = $( '#js-countdown' ).data( 'endevent' );
		const countdown = new Date( endevent );

		function getRemainingTime( endtime ) {
		  const milliseconds = Date.parse( endtime ) - Date.parse( new Date( ) );
		  const seconds = Math.floor( ( milliseconds / 1000 ) % 60 );
		  const minutes = Math.floor( ( milliseconds / 1000 / 60 ) % 60 );
		  const hours = Math.floor( ( milliseconds / ( 1000 * 60 * 60 ) ) % 24 );
		  const days = Math.floor( milliseconds / ( 1000 * 60 * 60 * 24 ) );

		  return {
			'total': milliseconds,
			'seconds': seconds,
			'minutes': minutes,
			'hours': hours,
			'days': days,
		  };
		}
		  
		function initClock( id, endtime ) {
		  const counter = document.getElementById( id );
		  if ( counter == undefined ) {
			return false;
		  }
		  const daysItem = counter.querySelector( '.js-countdown-days' );
		  const hoursItem = counter.querySelector( '.js-countdown-hours' );
		  const minutesItem = counter.querySelector( '.js-countdown-minutes' );
		  const secondsItem = counter.querySelector( '.js-countdown-seconds' );
		  const dayCountdown = counter.querySelector( '#day-countdown' );

		  function updateClock( ) {
			const time = getRemainingTime( endtime );

			daysItem.innerHTML = time.days;
			hoursItem.innerHTML = ( '0' + time.hours ).slice( -2 );
			minutesItem.innerHTML = ( '0' + time.minutes ).slice( -2 );
			secondsItem.innerHTML = ( '0' + time.seconds ).slice( -2 );
			dayCountdown.innerHTML = langDay( time.days );
			if ( time.total <= 0 ) {
			  clearInterval( timeinterval );
			}
		  }

		  updateClock( );
		  const timeinterval = setInterval( updateClock, 1000 );
		}

		initClock( 'js-countdown', countdown );


		function langDay( num ) {
			var lang = 'Дней', last_symbol;
			if ( num % 10 == 1 && num != 11 ) {
				lang = 'День';
			}
			num += '';
			last_symbol = num.substr( -1 );
			if ( ( last_symbol == 2 || last_symbol == 3 || last_symbol == 4 ) && num != 12 && num != 13 && num != 14 ) {
				lang = 'Дня';
			}
			return lang;
		}

	});

	$( window ).on( 'load', function( ) {
		var endevent = $( '#js-countdown2' ).data( 'endevent' );
		const countdown = new Date( endevent );

		function getRemainingTime( endtime ) {
		  const milliseconds = Date.parse( endtime ) - Date.parse( new Date( ) );
		  const seconds = Math.floor( ( milliseconds / 1000 ) % 60 );
		  const minutes = Math.floor( ( milliseconds / 1000 / 60 ) % 60 );
		  const hours = Math.floor( ( milliseconds / ( 1000 * 60 * 60 ) ) % 24 );
		  const days = Math.floor( milliseconds / ( 1000 * 60 * 60 * 24 ) );

		  return {
			'total': milliseconds,
			'seconds': seconds,
			'minutes': minutes,
			'hours': hours,
			'days': days,
		  };
		}
		  
		function initClock( id, endtime ) {
		  const counter = document.getElementById( id );
		  if ( counter == undefined ) {
			return false;
		  }
		  const daysItem = counter.querySelector( '.js-countdown-days2' );
		  const hoursItem = counter.querySelector( '.js-countdown-hours2' );
		  const minutesItem = counter.querySelector( '.js-countdown-minutes2' );
		  const secondsItem = counter.querySelector( '.js-countdown-seconds2' );
		  const dayCountdown = counter.querySelector( '#day-countdown2' );

		  function updateClock( ) {
			const time = getRemainingTime( endtime );

			daysItem.innerHTML = time.days;
			hoursItem.innerHTML = ( '0' + time.hours ).slice( -2 );
			minutesItem.innerHTML = ( '0' + time.minutes ).slice( -2 );
			secondsItem.innerHTML = ( '0' + time.seconds ).slice( -2 );
			dayCountdown.innerHTML = langDay( time.days );
			if ( time.total <= 0 ) {
			  clearInterval( timeinterval );
			}
		  }

		  updateClock( );
		  const timeinterval = setInterval( updateClock, 1000 );
		}

		initClock( 'js-countdown2', countdown );


		function langDay( num ) {
			var lang = 'Дней', last_symbol;
			if ( num % 10 == 1 && num != 11 ) {
				lang = 'День';
			}
			num += '';
			last_symbol = num.substr( -1 );
			if ( ( last_symbol == 2 || last_symbol == 3 || last_symbol == 4 ) && num != 12 && num != 13 && num != 14 ) {
				lang = 'Дня';
			}
			return lang;
		}

	});


})(jQuery)



