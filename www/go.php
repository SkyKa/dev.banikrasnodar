<?php

session_start();
include "config.php";
include "cms.php";

$id = substr(Utils::getGet('id'),0,-5);

$table = New table('redirection');
$insert = $table->getEntity($id);

$rows = $table->select( "SELECT url,count FROM `redirection` WHERE `id`=:id LIMIT 1 " , array( 'id' => $id ) );
$url = $rows[0]['url'];

if (!isset($rows[0]['count'])) $rows[0]['count']= 0;

$insert->count = $rows[0]['count'] + 1;
$table->save( $insert );

header("Location: $url");

?>
