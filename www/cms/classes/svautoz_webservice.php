<?php

class Svautoz_webservice {

	protected $_url = 'http://www.svautoz.ru/webservice/';

	protected $_format = array( 'url', 'xml', 'json', 'array', 'object' );

	protected $_sole = '';
	
	protected $_in;
	protected $_out;
	
	protected $_version; // not_used

	protected $_login;
	protected $_password;
	
	protected $_data;
	protected $_method;
	
		
	public function __construct( $conf ) {

		if ( isset( $conf[ 'in' ] ) ) $conf[ 'in' ] = strtolower( $conf[ 'in' ] );
		if ( isset( $conf[ 'out' ] ) ) $conf[ 'out' ] = strtolower( $conf[ 'out' ] );

		// 'url', 'xml', 'json', 'array'
		if ( isset( $conf[ 'in' ] ) && ( $conf[ 'in' ] == 'url' || $conf[ 'in' ] == 'xml' || $conf[ 'in' ] == 'json' ) ) $this -> _in = $conf[ 'in' ];
		else $this -> _in = 'url';

		if ( isset( $conf[ 'out' ] ) && array_search( strtolower( $conf[ 'out' ] ), $this -> _format ) !== false ) $this -> _out = $conf[ 'out' ];
		else $this -> _out = 'url';

		if ( !isset( $conf[ 'login' ] ) ) throw new Exception( 'not_used conf[login]' );
		else $this -> _login = $conf[ 'login' ];

		if ( !isset( $conf[ 'password' ] ) ) throw new Exception( 'not_used conf[password]' );
		else $this -> _password = $conf[ 'password' ];

	}


	/**	Т Е С Т О В А Я    К О М А Н Д А
		*/
	public function testConnetion( $str='' ) {
		$req = array(	'login' => base64_encode( $this -> _login ),
								'password' => base64_encode( $this -> _password ),
								'method' => 'testConnection',
								'data' => $str
		);
		$return = $this -> request( $req );
		return $return;
	}


	/**	К О М А Н Д А    П Р О Ц Е Н К И 
		*	art (string)		- Артикул искомой детали
		*	brand (string)	- Производитель
		*	cross (bool)		- true в случае, если нужно произвести поиск так же и по деталям, являющимся аналогами искомой детали,
		* 							  false — если поиск только по заданному артикулу (без аналогов)
		*/

	public function getPrice( $art, $brand=null, $cross=true ) {
		if ( !$art ) throw new Exception( 'you must used argument "art"' );
		$req = array(	'login' => base64_encode( $this -> _login ),
								'password' => base64_encode( $this -> _password ),
								'method' => 'getPrice',
								'data' => ''
		);
		$req[ 'data' ][ 'art' ] = $art;
		if ( isset( $brand ) && $brand ) $req[ 'data' ][ 'brand' ] = $brand;
		if ( $cross ) $req[ 'data' ][ 'cross' ] = $cross;
		$return = $this -> request( $req );
		return $return;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function request( $req ) {
		// не работает
		if ( $this -> _in == 'url' ) {
			$req = http_build_query( $req, '' );

		}
		// good
		else if ( $this -> _in == 'xml' ) {
			$req = self :: ArraytoXml( $req, 'request' );
			$ex_req = explode( "\n", $req );
			if ( isset( $ex_req[ 1 ] ) ) {
				$req = $ex_req[ 1 ];
				unset( $ex_req );
			}
		}
		// good
		else if ( $this -> _in == 'json' ) {
			$req = json_encode( $req );

		}

		$req = str_replace( "\n", '', $req );
		$req = str_replace( "\r", '', $req );
		$req = str_replace( "\t", '', $req );

		//var_dump( $req );
		$req = urlencode( $req );
		$req = base64_encode( $req );

		//var_dump( $this -> _url . '?out=' . $this -> _out . '&in=' . $this -> _in . '&data=' . $req );
		
		if ( $this -> _out == 'array' || $this -> _out == 'object' ) {
			$out_format = 'json';
		}
		else {
			$out_format = $this -> _out;
		}

		$content = file_get_contents( $this -> _url . '?out=' . $out_format . '&in=' . $this -> _in . '&data=' . $req );

		if ( $this -> _out == 'array' ) {
			$content = json_decode( $content, true );
			if ( !isset( $content[ 'data' ][ 'products' ] ) ) return $content;
			foreach( $content[ 'data' ][ 'products' ] as $key => $product ) {
				if ( mb_strlen( $product[ 'idProduct' ], 'UTF-8' ) > 50 ) {
					$service = unserialize( base64_decode( $product[ 'idProduct' ] ) );
					$service = unserialize( base64_decode( $service[ 'idExternal' ] ) );
					unset( $service[ 'pricerur' ] ); // цена тут не нужна
					$content[ 'data' ][ 'products' ][ $key ][ 'service' ] = $service;
				}
			}
			return $content;
		}
		if ( $this -> _out == 'object' ) {
			$content = json_decode( $content );
			return $content;
		}
		
		return $content;
	}

	
	
	
	
	
	public static function ArraytoXml( $data, $rootNodeName = 'data', $xml=null ) {

		// включить режим совместимости, не совсем понял зачем это но лучше делать
		if ( ini_get( 'zend.ze1_compatibility_mode' ) == 1 ) {
			ini_set ( 'zend.ze1_compatibility_mode', 0 );
		}

		if ( $xml == null ) {
			$xml = simplexml_load_string( "<$rootNodeName />" );
		}

		//цикл перебора массива
		foreach( $data as $key => $value )
		{
			// нельзя применять числовое название полей в XML
			if ( is_numeric( $key ) ) {
				// поэтому делаем их строковыми
				$key = "unknownNode_" . ( string ) $key;
			}

			// удаляем не латинские символы
			$key = preg_replace( '/[^a-z0-9]/i', '', $key );

			// если значение массива также является массивом то вызываем себя рекурсивно
			if ( is_array( $value ) ) {
				$node = $xml -> addChild( $key );
				// рекурсивный вызов
				self :: ArraytoXml( $value, $rootNodeName, $node );
			}
			else {
				// добавляем один узел
				$value = htmlentities( $value );
				$xml -> addChild( $key, $value );
			}

		}
		// возвратим обратно в виде строки  или просто XML-объект
		return $xml->asXML();
	}

}




