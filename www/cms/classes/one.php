<?php

$path_cms = realpath( dirname( __FILE__ ) . '/../../' ) . DIRECTORY_SEPARATOR;

require $path_cms . 'config.php';
require $path_cms . 'cms.php';



class Parser {

	// ����������� �������������
	static $views_allow = array(

		'pages' => array( 'show' ),
		'catalog' => array( 'show' ),
		'banner' => array( 'show' ),
		'infoblock' => array( 'show' )

	);


	// ������ ��������
	public static function run( $tpl='', $args=array() ) {

		$pattern = '/\{(.*)\}{1,}/i';
		preg_match_all( $pattern, $tpl, $matches );
		$matches = end( $matches );
		foreach ( $matches as $match ) {
			$match = mb_strtolower( $match );
			$tpl = self :: args( $match, $args, $tpl );
			$tpl = self :: view( $match, $tpl );
			$tpl = self :: classes( $match, $tpl );
			$tpl = self :: register( $match, $tpl );
			$tpl = self :: bd( $match, $tpl );
			$tpl = self :: view( $match, $tpl );
			
			
			$tpl = self :: procedure( $match, $tpl );
		}

		return $tpl;

	}

	// ���������� ���������
	public static function args( $match, $args, $tpl ) {
		if ( !count( $args ) ) return $tpl;
		if ( !isset( $args[ $match ] ) ) {
			$pattern = '/\{(.*)\}/i';
			preg_match_all( $pattern, $match, $matches );
			$matches = end( $matches );
			foreach ( $matches as $m ) {
				if ( isset( $args[ $m ] ) ) $tpl = str_replace( '{' . $m . '}', $args[ $m ], $match );
			}
			return $tpl;
		}
		else return str_replace( '{' . $match . '}', $args[ $match ], $tpl );
	}

	// ��������� �������������
	public static function view( $match, $tpl ) {

		$ex = explode( '.', $match );
		if ( count( $ex ) !== 3 ) return $tpl;

		if ( !isset( self :: $views_allow[ $ex[ 0 ] ] ) ) return $tpl;
		$view = false;
		foreach( self :: $views_allow[ $ex[ 0 ] ] as $a ) {
			if ( $ex[ 1 ] === $a ) {
				$view = true;
				break;
			}
		}
		try {
			$args = array( );
			/////////////////////
			/** ��������� �������������
			 * {BANNER.SHOW.PHONE|a=asd,b=3,c=1}
			 */ 
			$ex_args = explode( '|', $match );
			if ( count( $ex_args ) > 0 ) {
				$view = $ex_args[ 0 ];
				$ex_args = end( $ex_args );
				$ex_args = explode( ',', $ex_args );
				foreach( $ex_args as $param ) {
					$ex_param = explode( '=', $param );
					if ( count( $ex_param ) != 2 ) break;
					$args[ $ex_param[ 0 ] ] = $ex_param[ 1 ];
				}
			}
			else $view = $match;
			///////////////////////////////
			$replace = val( $view, $args );
			if ( $replace ) $tpl = str_replace( '{' . mb_strtoupper( $match, 'UTF-8' ) . '}', $replace, $tpl );
		} catch ( Exception $e ) {
			;
		}
		return $tpl;

	}

	// ������ � ������
	public static function classes( $match, $tpl ) {
		return $tpl;
	}

	// ��������� ��������� ������� Registry
	public static function register( $match, $tpl ) {
		return $tpl;
	}

	// ���� ������
	public static function bd( $match, $tpl ) {
		return $tpl;
	}



	// �������
	public static function procedure( $match, $tpl ) {
		return $tpl;
	}
	
	
	
}


	$tpl = '������ ���� �������� {BANNER.SHOW.PHONE|a=asd,b=3,z={A}} ���!';

	echo Parser :: run( $tpl, array('a'=>'66777333') );
