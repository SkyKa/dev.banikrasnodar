<?php


class Parser {

	// разрешенные представления
	static $views_allow = array(

		'pages' => array( 'show' ),
		'catalog' => array( 'show' ),
		'banner' => array( 'show' ),
		'infoblock' => array( 'show' )

	);


	// запуск парсинга
	public static function run( $tpl='', $args=array() ) {

		$pattern = '/\{(.*)\}{1,}/i';
		preg_match_all( $pattern, $tpl, $matches );
		$matches = end( $matches );
		foreach ( $matches as $match ) {
			$match = mb_strtolower( $match );
			$tpl = self :: args( $match, $args, $tpl );
			$tpl = self :: view( $match, $tpl );
			$tpl = self :: classes( $match, $tpl );
			$tpl = self :: registry( $match, $tpl );
			$tpl = self :: db( $match, $tpl );
			$tpl = self :: view( $match, $tpl );

			$tpl = self :: procedure( $match, $tpl );
		}

		return $tpl;

	}

	// переданные аргументы
	public static function args( $match, $args, $tpl ) {
		if ( !count( $args ) ) return $tpl;
		if ( !isset( $args[ $match ] ) ) {
			$pattern = '/\{(.*)\}/i';
			preg_match_all( $pattern, $match, $matches );
			$matches = end( $matches );
			foreach ( $matches as $m ) {
				if ( isset( $args[ $m ] ) ) $tpl = str_replace( '{' . $m . '}', $args[ $m ], $match );
			}
			return $tpl;
		}
		else return str_replace( '{' . $match . '}', $args[ $match ], $tpl );
	}

	// созданные представления
	public static function view( $match, $tpl ) {

		$ex = explode( '.', $match );
		if ( count( $ex ) !== 3 ) return $tpl;

		if ( !isset( self :: $views_allow[ $ex[ 0 ] ] ) ) return $tpl;
		$view = false;
		foreach( self :: $views_allow[ $ex[ 0 ] ] as $a ) {
			if ( $ex[ 1 ] === $a ) {
				$view = true;
				break;
			}
		}
		try {
			$args = array( );
			/////////////////////
			/** аргументы представлений
			 * {BANNER.SHOW.PHONE|a=asd,b=3,c=1}
			 */ 
			$ex_args = explode( '|', $match );
			if ( count( $ex_args ) > 0 ) {
				$view = $ex_args[ 0 ];
				$ex_args = end( $ex_args );
				$ex_args = explode( ',', $ex_args );
				foreach( $ex_args as $param ) {
					$ex_param = explode( '=', $param );
					if ( count( $ex_param ) != 2 ) break;
					$args[ $ex_param[ 0 ] ] = $ex_param[ 1 ];
				}
			}
			else $view = $match;
			///////////////////////////////
			$replace = val( $view, $args );
			if ( $replace ) $tpl = str_replace( '{' . mb_strtoupper( $match, 'UTF-8' ) . '}', $replace, $tpl );
		} catch ( Exception $e ) {
			;
		}
		return $tpl;

	}

	// классы и методы
	public static function classes( $match, $tpl ) {
		return $tpl;
	}

	// параметры созданные классом Registry
	public static function registry( $match, $tpl ) {
		return $tpl;
	}

	// база данных
	public static function db( $match, $tpl ) {
		return $tpl;
	}



	// функции
	public static function procedure( $match, $tpl ) {
		return $tpl;
	}
	
	
	
}