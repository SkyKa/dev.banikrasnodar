<?php
 /**
  * Autor:		Startcev PV
  * Email:		cefar@mail.ru
  * Phone:		+7(912)323-84-94
  * Company:	IT-FACTORY
  */
	class Acquiring {

		// синглтон
		private static $_instance; // object 

		public static function & __instance( $conf ) {
			if ( !isset( self :: $_instance ) ) {
				if ( empty( $conf[ 'class' ] ) || !$conf[ 'class' ] ) {
					throw new Exception( 'params class not found' );
				}
				else {
					$class = $conf[ 'class' ];
					unset( $conf[ 'class' ] );
					self :: $_instance = new $class( $conf );
				}
			}
			return self :: $_instance;
		}

		public function __construct( $conf ) {
		

		}
	
		public function __destruct( ) {
			;
		}

	}