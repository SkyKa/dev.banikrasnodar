<?php
/*
 * Контроллер cm перенаправления
 */

class RedirectionController_Cm extends Controller_Base
{
    public function access()
    {
        return $this->login();
    }

    public function index()
    {
    }

    public function navigator()
    {
        $template = $this->createTemplate();

        $template->render();
    }

    public function editor()
    {
        $id = Utils::getVar('id');

        $data = $this->getData();

        $object = $data->getObject($id);

        if($object)
        {
            $template = $this->createTemplate();
            $template->redirection = $object;
            $template->render();
        }
    }

    public function tree()
    {
        $template = $this->createTemplate();
        $data = $this->getData();
        $redirections = $data->redirections;
        $template->redirections = $redirections;
        $template->render();
    }

    public function add()
    {
        $res = array();

        try
        {
            $data = $this->getData();
            $object = $data->getObject();
           
            $title = Utils::getPost('title');
            $alias = Utils::translit($title);
            $alias = Utils::getUniqueAlias($alias, "redirection");
            $object->name = $title;
            $object->alias = $alias;
            
            $data->save($object);

            $res['success'] = true;
            $res['msg'] = 'Добавлено';
        }
        catch(Exception $e)
        {
            $res['success'] = false;
            $res['msg'] = $e->getMessage();
        }

        $this->setContent(json_encode($res));
    }

    public function save()
    {
       
        $res = array();

        try
        {
            $data = $this->getData();

            $id = Utils::getVar('id');

            $object = $data->getObject($id);

            if($object)
            {
                $object->name = Utils::getPost('name');
                $object->alias = Utils::getPost('alias');
                $object->url = Utils::getPost('url');

                $data->save($object);
                
                $errorInfo = $data->table->errorInfo;
                if($errorInfo)
                {
                    throw new Exception($errorInfo);
                }
              
            }
            else
            {
                throw new Exception('Объект уже удален');
            }
            $res['success'] = true;
            $res['msg'] = 'Сохранено';
        }
        catch(Exception $e)
        {
            $res['success'] = false;
            $res['msg'] = $e->getMessage();
        }

        $this->setContent(json_encode($res));
    }

    public function delete()
    {
        $res = array();

        try
        {
            $data = $this->getData();

            $id = Utils::getVar('id');

            $object = $data->getObject($id);

            if($object)
            {
                $data->table->delete($object);
                $errorInfo = $data->table->errorInfo;
                if($errorInfo)
                {
                    throw new Exception($errorInfo);
                }
            }
            else
            {
                throw new Exception('Объект уже удален');
            }
            $res['success'] = true;
            $res['msg'] = 'Удалено';
        }
        catch(Exception $e)
        {
            $res['success'] = false;
            $res['msg'] = $e->getMessage();
        }

        $this->setContent(json_encode($res));
    }
}

?>
