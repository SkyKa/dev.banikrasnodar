<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Контент менеджер - <?php echo htmlspecialchars(Registry::__instance()->site_name) ?></title>
        <base href="<?php echo $base ?>" />
        <link rel="stylesheet" type="text/css" href="cms/modules/mysite/html/css/application.css" />
        <style type="text/css">
            html, body {
                margin:0;
                padding:0;
                border:0 none;
                overflow:hidden;
                height:100%;
            }
        </style>
    </head>
    <body scroll="no" id="docs">
        <div id="loading-mask" style="width:100%;height:100%;background:#c3daf9;position:absolute;z-index:20000;left:0;top:0;">&#160;</div>
        <div id="loading">
            <div class="loading-indicator"><img src="jscripts/ext/resources/images/default/grid/loading.gif" style="width:16px;height:16px;" align="absmiddle">&#160;Загрузка...</div>
        </div>

        <link rel="stylesheet" type="text/css" href="jscripts/ext/resources/css/ext-all.css" />

        <link rel="stylesheet" type="text/css" href="jscripts/ext/ux/css/CenterLayout.css" />

        <link rel="stylesheet" type="text/css" href="cms/modules/mysite/html/css/application.css" />

        <link rel="stylesheet" type="text/css" href="cms/modules/mysite/html/css/chooser.css" />

        <link rel="stylesheet" type="text/css" href="jscripts/bootstrap.css" />

        <link rel="stylesheet" type="text/css" href="jscripts/style.css" />

        <script type="text/javascript" src="jscripts/ext/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="jscripts/ext/ext-all.js"></script>

        <script type="text/javascript" src="jscripts/ext/src/locale/ext-lang-ru.js"></script>

        <!-- extensions -->
        <script type="text/javascript" src="jscripts/ext/ux/CenterLayout.js"></script>
        <script type="text/javascript" src="jscripts/ext/ux/RowLayout.js"></script>
        <script type="text/javascript" src="jscripts/ext/ux/TabCloseMenu.js"></script>
        <script type="text/javascript" src="jscripts/ext/ux/FileUploadField.js"></script>
        <script type="text/javascript" src="jscripts/ext/ux/XmlTreeLoader.js"></script>

        <link rel="stylesheet" type="text/css" href="jscripts/ext/ux/css/fileuploadfield.css"/>

        <!--<script type="text/javascript" src="jscripts/fckeditor/fckeditor.js"></script>-->
        <script type="text/javascript" src="jscripts/ckeditor/ckeditor.js"></script>

        <script type="text/javascript" src="cms/modules/mysite/html/js/application.js"></script>
        <script type="text/javascript" src="cms/modules/catalog/html/js/editor.js"></script>
<!--
        <script type="text/javascript" src="jscripts/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="jscripts/bootstrap.js"></script>
        <script type="text/javascript" src="jscripts/inheritance.js"></script>
        <script type="text/javascript" src="jscripts/jquery.tmpl.js"></script>
        <script type="text/javascript" src="jscripts/action.js"></script>

        <script id="modal-header-template" type="html/template">
            <div class="row">
                <h4>{{=it.header}}</h4>
            </div>
        </script>

        <script id="modal-item-template" type="html/template">

            <span>
                <input data-id="{{=it.lesson.id }}" data-user="{{=it.user_id }}" {{=(it.lesson.credit == 1 ? 'checked': '') }} type="checkbox"/>
                <span>{{=it.lesson.title }}</span>
            </span>

        </script>

        <script id="modal-template" type="html/template">

            <div class="modal fade" id="lessons_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Вопросы</h4>
                  </div>
                  <div class="modal-body">
                    


                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                  </div>
                </div>
              </div>
            </div>

        </script>
-->
        <div id="header">
            <h1><?php echo Config::__("mysite")->title?> v.<?php mod("mysite.version")?> - <?php echo htmlspecialchars(Registry::__instance()->site_name) ?></h1>
            <a id="onexit" href="#">Выход(<?php $user=val('security.login.user'); echo $user['name']; ?>)</a>
            <a id="onsite" href="" target="_blank">На сайт</a>
        </div>
    </body>
</html>
