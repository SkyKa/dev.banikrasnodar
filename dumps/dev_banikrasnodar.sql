-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 05 2018 г., 19:56
-- Версия сервера: 5.7.15
-- Версия PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dev.banikrasnodar`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `alias`, `name`, `html`) VALUES
(1, 'phone', 'Телефон', '+7 908-8288-828'),
(2, 'e-mail', 'Электронная почта', 'info@npoprogress.ru'),
(3, 'contacts-production', 'Контакты-производство', 'Краснодарский край,\nг. Краснодар,\nул. Мостовая, 1'),
(4, 'contacts-department', 'Контакты-отдел продаж', 'Краснодарский край,\nУсть-Лабинский район,\nп. Двубратский, ул. Мостовая, 1');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_section`
--

CREATE TABLE `catalog_section` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `children_tpl` text COLLATE utf8_unicode_ci,
  `leaf` smallint(6) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `catalog_section`
--

INSERT INTO `catalog_section` (`id`, `parent_id`, `title`, `position`, `alias`, `position_table`, `section_table`, `children_tpl`, `leaf`) VALUES
(1, NULL, 'Формы', 1, 'forms_section', NULL, 'section_forms_settings', '<tpl>\r\n  <section>section_forms</section>\r\n  <position>position_forms</position>\r\n  <leaf>1</leaf>\r\n</tpl>', NULL),
(2, NULL, 'Каталог', 2, 'katalog', NULL, NULL, NULL, NULL),
(3, 1, 'Оставить заявку', 1, 'feedback_form', 'position_forms', 'section_forms', '', 1),
(4, 1, 'Заказать', 2, 'order_form', 'position_forms', 'section_forms', '', 1),
(5, 1, 'Хочу бесплатную доставку', 3, 'hochu_besplatnuyu_dostavku', 'position_forms', 'section_forms', '', 1),
(7, 1, 'Получить скидку', 5, 'poluchit_skidku', 'position_forms', 'section_forms', '', 1),
(8, NULL, 'Слайдер', 4, 'slajder', 'position_slides', '', '', NULL),
(9, NULL, 'Слайдер компаний', 3, 'slajder-kompanij', 'position_compan', '', '', NULL),
(10, 2, 'БАНЯ-БОЧКА «ТРИО»', 2, 'banya-bochka-trio', '', 'section_product', '', NULL),
(21, 2, 'БАНЯ-БОЧКА «ДУО»', 1, 'banya-bochka-duo', '', 'section_product', '', NULL),
(20, 10, 'Тип бочки', 2, 'tip-bochki', 'position_bochka_type', '', '', NULL),
(13, 10, 'Галерея', 1, 'galereya', 'position_gallery', '', '', NULL),
(19, NULL, 'Характеристики(Фильтры)', 5, 'filters', 'filters', '', '', NULL),
(15, 10, 'Характеристики', 3, 'harakteristiki', 'position_characteristics', '', '', NULL),
(16, 10, 'Отзывы', 4, 'otzyvy', 'position_reviews', '', '', NULL),
(17, 10, 'Видео', 5, 'video', 'position_videos', '', '', NULL),
(22, 21, 'Галерея', 1, 'galereya2', 'position_gallery', '', '', NULL),
(23, 21, 'Характеристики', 2, 'harakteristiki2', 'position_characteristics', '', '', NULL),
(24, 21, 'Тип бочки', 3, 'tip-bochki2', 'position_bochka_type', '', '', NULL),
(25, 21, 'Отзывы', 4, 'otzyvy2', 'position_reviews', '', '', NULL),
(26, 21, 'Видео', 5, 'video2', 'position_videos', '', '', NULL),
(27, NULL, 'Часто задаваемые вопросы', 6, 'chasto-zadavaemye-voprosy', 'questions', '', '', NULL),
(28, 1, 'Оставить отзыв', 6, 'send_feedback', 'position_forms', 'section_forms', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `filters`
--

CREATE TABLE `filters` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filters`
--

INSERT INTO `filters` (`id`, `section_id`, `title`, `public`) VALUES
(1, 19, 'Толщина стенки', 1),
(2, 19, 'Диаметр бочки', 1),
(3, 19, 'Длина корпуса снаружи', 1),
(4, 19, 'Долговечность', 1),
(5, 19, 'Впитываемость', 1),
(6, 19, 'Уровень шума', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_action`
--

CREATE TABLE `forms_field_action` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_action`
--

INSERT INTO `forms_field_action` (`id`, `name`, `position`) VALUES
('auth', 'Авторизация', 2),
('is_active', 'Активация учетной запись', 4),
('register', 'Регистрация', 1),
('restore_pass', 'Восстановить пароль', 3),
('send_email', 'Отправка на email', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_type`
--

CREATE TABLE `forms_field_type` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_type`
--

INSERT INTO `forms_field_type` (`id`, `name`, `position`) VALUES
('check', 'Флаг', 7),
('date', 'Дата', 5),
('hidden', 'Скрытое поле', 11),
('html', 'Произвольный HTML', 15),
('info', 'Информация', 14),
('label', 'Подпись', 13),
('link', 'Ссылка', 10),
('memo', 'Многострочный текст', 2),
('pwd', 'Пароль', 6),
('radiobox', 'Радиобокс', 12),
('select', 'Выпадающий список', 3),
('submit', 'Кнопка подачи запроса', 8),
('text', 'Текстовая строка', 1),
('upload', 'Обзор', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `infoblock`
--

CREATE TABLE `infoblock` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `title`) VALUES
(3, 'header_menu', 'Верхнее меню'),
(4, 'footer_menu', 'Нижнее меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menus_item`
--

CREATE TABLE `menus_item` (
  `id` int(11) NOT NULL,
  `menus_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` smallint(6) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `type_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus_item`
--

INSERT INTO `menus_item` (`id`, `menus_id`, `title`, `parent_id`, `position`, `visible`, `type`, `type_id`, `type_link`, `attr`, `img_src`) VALUES
(1, 3, 'О компании', NULL, 1, 1, '_link', 8, '#about', '', NULL),
(2, 3, 'Каталог', NULL, 2, 1, '_link', 2, '#catalog', '', NULL),
(4, 3, 'Акции', NULL, 4, 1, '_link', 4, '#sale', '', NULL),
(5, 3, 'Отзывы', NULL, 5, 1, '_link', 5, '#rew', '', NULL),
(6, 3, 'Вопрос-ответ', NULL, 6, 1, '_link', 6, '#qa', '', NULL),
(7, 3, 'Контакты', NULL, 7, 1, '_link', 7, '#contact', '', NULL),
(8, 3, 'Как мы работаем', 1, 1, 1, '_pages', 9, '/kak-my-rabotaem.html', '', NULL),
(13, 4, 'О компании', NULL, 1, 1, '_link', 8, '#about', '', NULL),
(14, 4, 'Каталог', NULL, 2, 1, '_link', 2, '#catalog', '', NULL),
(16, 4, 'Акции', NULL, 4, 1, '_link', 4, '#sale', '', NULL),
(17, 4, 'Отзывы', NULL, 5, 1, '_link', 5, '#rew', '', NULL),
(18, 4, 'Вопрос-ответ', NULL, 6, 1, '_link', 6, '#qa', '', NULL),
(19, 4, 'Контакты', NULL, 7, 1, '_link', 7, '#contact', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `position` int(11) NOT NULL,
  `visible` smallint(6) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugins` text COLLATE utf8_unicode_ci,
  `mainpage` smallint(6) NOT NULL DEFAULT '0',
  `announcement` text COLLATE utf8_unicode_ci,
  `fileupload` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` smallint(6) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `title2`, `content`, `position`, `visible`, `parent_id`, `alias`, `template`, `plugins`, `mainpage`, `announcement`, `fileupload`, `img_src`, `redirect`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(1, 'Главная', NULL, '', 1, 0, NULL, 'index', 'main.tpl.php', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Каталог', '', NULL, 2, 1, NULL, 'catalog', 'catalog.tpl.php', '', 0, '', NULL, NULL, NULL, '', '', '', ''),
(4, 'Акции', '', NULL, 3, 1, NULL, 'shares', 'page.tpl.php', '', 0, '', NULL, NULL, NULL, '', '', '', ''),
(8, 'О компании', '', NULL, 4, 1, NULL, 'company', 'page.tpl.php', '', 0, '', NULL, NULL, NULL, '', '', '', ''),
(9, 'Как мы работаем', NULL, NULL, 1, 1, 8, 'kak-my-rabotaem', 'page.tpl.php', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Сотрудники', NULL, NULL, 2, 1, 8, 'sotrudniki', 'page.tpl.php', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Реквизиты', NULL, NULL, 3, 1, 8, 'rekvizity', 'page.tpl.php', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Обратная связь', NULL, NULL, 4, 1, 8, 'obratnaya-svyaz', 'page.tpl.php', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_bochka_type`
--

CREATE TABLE `position_bochka_type` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `new_price` varchar(255) DEFAULT NULL,
  `old_price` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_bochka_type`
--

INSERT INTO `position_bochka_type` (`id`, `section_id`, `type`, `new_price`, `old_price`) VALUES
(1, 20, 'Сосна', '115 000', '146 000'),
(2, 20, 'Кедр', NULL, '188 000'),
(3, 24, 'Сосна', '115 000 ', '146 000'),
(4, 24, 'Кедр', '120 000', '150 000');

-- --------------------------------------------------------

--
-- Структура таблицы `position_characteristics`
--

CREATE TABLE `position_characteristics` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ed` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_characteristics`
--

INSERT INTO `position_characteristics` (`id`, `section_id`, `title`, `type`, `ed`, `count`, `position`) VALUES
(1, 15, 'Толщина стенки', 'ГАБАРИТНЫЕ РАЗМЕРЫ', 'мм', 45, NULL),
(2, 15, 'Диаметр бочки', 'ГАБАРИТНЫЕ РАЗМЕРЫ', 'мм', 45, NULL),
(3, 15, 'Длина корпуса снаружи', 'ГАБАРИТНЫЕ РАЗМЕРЫ', 'мм', 45, NULL),
(4, 15, 'Долговечность', 'КАЧЕСТВЕННЫЕ ОСОБЕННОСТИ', NULL, 45, NULL),
(5, 15, 'Впитываемость', 'КАЧЕСТВЕННЫЕ ОСОБЕННОСТИ', NULL, 235, NULL),
(6, 15, 'Уровень шума', 'КАЧЕСТВЕННЫЕ ОСОБЕННОСТИ', NULL, 3, NULL),
(7, 23, 'Впитываемость', 'КАЧЕСТВЕННЫЕ ОСОБЕННОСТИ', NULL, 220, NULL),
(8, 23, 'Толщина стенки', 'ГАБАРИТНЫЕ РАЗМЕРЫ', 'мм', 40, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_compan`
--

CREATE TABLE `position_compan` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_compan`
--

INSERT INTO `position_compan` (`id`, `section_id`, `img`, `position`, `visible`) VALUES
(1, 9, 'files/catalog/upload/91717de15faaf1069b82540d37e15d3d.png', 10, 1),
(2, 9, 'files/catalog/upload/5cae511ff9f61ea5088e5a462b00d313.png', 20, 1),
(3, 9, 'files/catalog/upload/1382f20634e934f967abf737b9c645e7.png', 30, 1),
(4, 9, 'files/catalog/upload/3e2b7e2f6b11cf18b08fb7508994a0ae.png', 40, 1),
(5, 9, 'files/catalog/upload/336eafe0dda00715492479408538d3f0.png', 50, 1),
(6, 9, 'files/catalog/upload/596581a8685c6b28374f1dc9c1e6cb6f.png', 60, 1),
(11, 9, 'files/catalog/upload/74b83e48b63dac15a285cbd9e0e943fe.png', 10, 1),
(12, 9, 'files/catalog/upload/58ed5083ec1f7c241d43dc15d1d119fe.png', 20, 1),
(13, 9, 'files/catalog/upload/7af675850fd02cbb0afd1b4e413f6d82.png', 30, 1),
(14, 9, 'files/catalog/upload/0d1cf3e9b35e6cf5c495ce611118a97f.png', 40, 1),
(15, 9, 'files/catalog/upload/d2a93a9204d18324c072ac3fef5fa7cd.png', 50, 1),
(16, 9, 'files/catalog/upload/6fbb8861386bfda6e132fbcf42d2b58f.png', 60, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `position_forms`
--

CREATE TABLE `position_forms` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type_id` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_empty` smallint(6) DEFAULT '0',
  `valid_email` smallint(6) DEFAULT '0',
  `select_options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `nameid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_before` text,
  `html_after` text,
  `retailcrm_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_forms`
--

INSERT INTO `position_forms` (`id`, `section_id`, `name`, `position`, `type_id`, `valid_empty`, `valid_email`, `select_options`, `nameid`, `html_before`, `html_after`, `retailcrm_name`) VALUES
(1, 3, 'Имя', 10, 'text', 1, NULL, NULL, 'modal_name', '<div class="form-group">', NULL, NULL),
(2, 3, 'Телефон', 20, 'text', 1, NULL, NULL, 'modal_phone', NULL, NULL, NULL),
(3, 3, 'E-mail', 30, 'text', 1, 1, NULL, 'modal_mail', NULL, NULL, NULL),
(4, 3, NULL, 40, 'check', 1, NULL, NULL, 'privacy_modal', '<p>', '<label for="privacy_modal">Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a id="overlay" type="button">политикой конфиденциальности</a></label></p>', NULL),
(5, 3, 'Оставить заявку', 50, 'submit', NULL, NULL, NULL, NULL, NULL, '</div>', NULL),
(6, 28, 'Представьтесь пожалуйста', 10, 'text', 1, NULL, NULL, 'name', '<div class="form-group">', NULL, NULL),
(7, 28, 'Из какого вы города?', 20, 'text', 1, NULL, NULL, 'place', NULL, NULL, NULL),
(8, 28, 'Оставьте ваш отзыв', 30, 'memo', 1, NULL, NULL, 'feedback', NULL, NULL, NULL),
(9, 28, NULL, NULL, 'hidden', 1, NULL, NULL, 'sid', NULL, NULL, NULL),
(10, 28, 'Оставить отзыв', 40, 'submit', NULL, NULL, NULL, NULL, NULL, '</div>', NULL),
(11, 28, 'Срок изготовления', 25, 'text', NULL, NULL, NULL, 'count', NULL, NULL, NULL),
(12, 5, 'E-mail', 10, 'text', 1, 1, NULL, 'email', '<div class="form-group">', NULL, NULL),
(13, 5, 'Телефон', 20, 'text', 1, NULL, NULL, 'phone', NULL, NULL, NULL),
(14, 5, NULL, 40, 'check', 1, NULL, NULL, 'privacy_offer', '<p>', '<label for="privacy_offer">Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a id="overlay2" type="button">политикой конфиденциальности</a></label></p>', NULL),
(15, 5, 'Хочу бесплатную доставку', 50, 'submit', NULL, NULL, NULL, NULL, NULL, '</div>', NULL),
(16, 7, 'Имя', 10, 'text', 1, NULL, NULL, 'name_2', '<div class="form-group">\n<div class="col-xs-12 col-sm-3">', '</div>', NULL),
(17, 7, 'Телефон', 20, 'text', 1, NULL, NULL, 'tel_2', '<div class="col-xs-12 col-sm-3">', '</div>', NULL),
(18, 7, 'E-mail', 30, 'text', 1, 1, NULL, 'mail_2', '<div class="col-xs-12 col-sm-3">', '</div>', NULL),
(19, 7, 'Получить скидку', 40, 'submit', NULL, NULL, NULL, NULL, '<div class="col-xs-12 col-sm-3">', '</div>', NULL),
(20, 7, NULL, 50, 'check', 1, NULL, NULL, 'privacy_offer_2', '<p>', '<label for="privacy_offer_2">Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a id="overlay3" type="button">политикой конфиденциальности.</a></label></p>\n</div>', NULL),
(21, 4, 'Имя', 10, 'text', 1, NULL, NULL, 'modal_name', '<div class="form-group">', NULL, NULL),
(22, 4, 'Телефон', 20, 'text', 1, NULL, NULL, 'modal_phone', NULL, NULL, NULL),
(23, 4, 'E-mail', 30, 'text', 1, 1, NULL, 'modal_mail', NULL, NULL, NULL),
(24, 4, 'Комментарий', 40, 'memo', 1, NULL, NULL, 'comment', NULL, NULL, NULL),
(25, 4, 'Заказать', 50, 'submit', NULL, NULL, NULL, NULL, NULL, '</div>', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_gallery`
--

CREATE TABLE `position_gallery` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `description` text,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_gallery`
--

INSERT INTO `position_gallery` (`id`, `section_id`, `img`, `description`, `position`) VALUES
(1, 13, 'files/catalog/upload/a110c177075bc228d7644e1db14b6690.jpg', '1', 10),
(2, 13, 'files/catalog/upload/2f141cb56531cb3c981b3f1539bcc085.jpg', '2', 20),
(3, 13, 'files/catalog/upload/d296c5d741f6d78ab7e77db89c24cae2.png', '3', 30),
(4, 22, 'files/catalog/upload/f76e7d2fb87d04b5c462b88f9a69d57c.png', '1', 10),
(5, 22, 'files/catalog/upload/535b4c843f129d61a02bee85cce31acc.jpg', '2', 20),
(6, 22, 'files/catalog/upload/3c8b69a087e5fe58c4dd699ec9827aea.jpg', '30', 3),
(7, 13, 'files/catalog/upload/5fed99a1290e24f9a7c8de280f6987ea.jpg', '5', 40);

-- --------------------------------------------------------

--
-- Структура таблицы `position_news`
--

CREATE TABLE `position_news` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datestamp` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `position_reviews`
--

CREATE TABLE `position_reviews` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `datestamp` int(15) DEFAULT NULL,
  `count` varchar(255) DEFAULT NULL,
  `feedback` text,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_reviews`
--

INSERT INTO `position_reviews` (`id`, `section_id`, `name`, `place`, `datestamp`, `count`, `feedback`, `public`) VALUES
(1, 25, 'Евгений Савельев', 'г. Краснодар', 1516460100, '1 день', '	Кутана неустойчива. Парафинирование мгновенно. Осушение ненаблюдаемо трансформирует фраджипэн. Горизонт отталкивает карбонат кальция, что дает возможность использования данной методики.', 1),
(2, 16, 'Евгений Савельев', 'г. Краснодар', 1516460160, '7 дней', '	Кутана неустойчива. Парафинирование мгновенно. Осушение ненаблюдаемо трансформирует фраджипэн. Горизонт отталкивает карбонат кальция, что дает возможность использования данной методики.', 1),
(3, 16, 'Test', 'test', 1515682680, '15 дней', 'test test test', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `position_setting`
--

CREATE TABLE `position_setting` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `position_slides`
--

CREATE TABLE `position_slides` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `title2` varchar(250) DEFAULT NULL,
  `description2` text,
  `position` int(11) DEFAULT NULL,
  `visible` tinyint(2) DEFAULT NULL,
  `timer` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_slides`
--

INSERT INTO `position_slides` (`id`, `section_id`, `img`, `title`, `description`, `title2`, `description2`, `position`, `visible`, `timer`) VALUES
(1, 8, 'files/catalog/upload/82a1dbae46636ad17aeac47a34fc172b.jpg', 'Купить баню весной', '<p>и получить скидку 20%</p>\n', NULL, NULL, 10, 1, 1),
(2, 8, 'files/catalog/upload/dd1c32fdb9ca7331c7e75d8dccf5b47f.jpg', 'Купить баню весной', '<p>и получить скидку 30%</p>\n', 'Тест', '<p>тесттест</p>\n', 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_videos`
--

CREATE TABLE `position_videos` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `video` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `datestamp` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_videos`
--

INSERT INTO `position_videos` (`id`, `section_id`, `video`, `title`, `datestamp`) VALUES
(1, 17, 'https://www.youtube.com/embed/RX40E5CUfP0', 'Как происходит монтаж бани. Особенности и сикреты', 1505666940),
(2, 17, 'https://www.youtube.com/embed/RX40E5CUfP0', 'Как происходит монтаж бани. Особенности и сикреты', 1505666940),
(3, 17, ' https://www.youtube.com/embed/RX40E5CUfP0', 'Как происходит монтаж бани. Особенности и сикреты', 1505667000),
(4, 26, 'https://www.youtube.com/embed/RX40E5CUfP0', 'Отделка бани.', 1516439640);

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `question` text,
  `answer` text,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `section_id`, `question`, `answer`, `position`) VALUES
(6, 27, 'Через какое время после установки мобильной бани можно её эксплуатировать?', '<p>Слив воды происходит за счет расположенных в полу (под доской) оцинкованных ёмкостей, по ним вода стекает в подготовленную трубу, затем в канализацию или на улицу.</p>\n', 10),
(7, 27, '<p>Через какое время после установки мобильной бани можно её эксплуатировать?</p>\n', '<p>Слив воды происходит за счет расположенных в полу (под доской) оцинкованных ёмкостей, по ним вода стекает в подготовленную трубу, затем в канализацию или на улицу.</p>\n', 20),
(8, 27, '<p>Через какое время после установки мобильной бани можно её эксплуатировать?</p>\n', '<p>Слив воды происходит за счет расположенных в полу (под доской) оцинкованных ёмкостей, по ним вода стекает в подготовленную трубу, затем в канализацию или на улицу.</p>\n', 30);

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms`
--

CREATE TABLE `section_forms` (
  `id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captcha` smallint(6) DEFAULT '0',
  `header_mail` text COLLATE utf8_unicode_ci,
  `title_form` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8_unicode_ci,
  `html` text COLLATE utf8_unicode_ci,
  `html_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_form` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `save_table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_order_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL,
  `ya_metrika_target_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_target_id_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms`
--

INSERT INTO `section_forms` (`id`, `email`, `efrom`, `efromname`, `esubject`, `captcha`, `header_mail`, `title_form`, `success_message`, `html`, `html_id`, `class_form`, `method`, `action`, `save_table`, `retailcrm_order_method`, `retailcrm_on`, `ya_metrika_target_id`, `ya_metrika_target_id_button`, `ya_metrika_on`) VALUES
(3, 'testmailitf@yandex.ru', NULL, NULL, 'Бани->Оставить заявку', 0, NULL, NULL, 'Спасибо! Ваша заявка обрабатывается нашими специалистами.', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>Почта: {EMAIL}</p>\n', 'f_feedback_form', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'testmailitf@yandex.ru', NULL, NULL, 'Бани->Заказать Баню', 0, NULL, NULL, 'Спасибо! Ваша заявка обрабатывается нашими специалистами.', NULL, 'f_order_form', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, 1),
(5, 'testmailitf@yandex.ru', NULL, NULL, 'Бани->Хочу бесплатную доставку', 0, NULL, NULL, 'Спасибо! Ваша заявка обрабатывается нашими специалистами.', NULL, 'f_hochu_besplatnuyu_dostavku', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'testmailitf@yandex.ru', NULL, NULL, 'Бани->Получить скидку', 0, NULL, NULL, 'Спасибо! Ваша заявка обрабатывается нашими специалистами.', NULL, 'f_poluchit_skidku', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'testmailitf@yandex.ru', NULL, NULL, 'БаниКраснодар -> Отзыв', 0, NULL, NULL, 'Спасибо за оставленный отзыв!', '<p>ФИО: {FIO}</p>\n\n<p>ГОРОД:{PLACE}</p>\n\n<p>ОТЗЫВ: {FEEDBACK}</p>\n\n<p></p>\n\n<p></p>\n', 'f_send_feedback', NULL, 'ajax-post', 'send_reviews', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms_settings`
--

CREATE TABLE `section_forms_settings` (
  `id` int(11) NOT NULL,
  `ya_metrika_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL,
  `retailcrm_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms_settings`
--

INSERT INTO `section_forms_settings` (`id`, `ya_metrika_key`, `ya_metrika_id`, `ya_metrika_on`, `retailcrm_key`, `retailcrm_on`) VALUES
(1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_form_feedback`
--

CREATE TABLE `section_form_feedback` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_news`
--

CREATE TABLE `section_news` (
  `id` int(11) NOT NULL,
  `page_size` int(11) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_page`
--

CREATE TABLE `section_page` (
  `id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `visible` smallint(6) DEFAULT '1',
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_product`
--

CREATE TABLE `section_product` (
  `id` int(14) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `sale` int(3) DEFAULT NULL,
  `new` tinyint(2) DEFAULT NULL,
  `ldescription` text,
  `description` text,
  `main_img` varchar(255) DEFAULT NULL,
  `visible` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section_product`
--

INSERT INTO `section_product` (`id`, `title`, `sale`, `new`, `ldescription`, `description`, `main_img`, `visible`) VALUES
(10, 'БАНЯ-БОЧКА «ТРИО»', 30, 1, '<p>ООО &laquo;НПО Прогресс&raquo; основан в 1998 году и оснащен современным технологическим оборудованием, приборами для входного контроля исходных материалов, операционного контроля изготовления литейных форм, термической и механической обработки, приемки готовой продукции. Собственные производственные помещения располагаются в г. Краснодар, г. Челябинск, г. Могилев РБ. Доставка готовой продукции производится по всей России и республике Беларусь на собственном транспорте ООО НПО &laquo;Прогресс&raquo;.</p>\n', 'НПО ПРОГРЕСС - БАНИ С ДОСТАВКОЙ\n\nООО «НПО Прогресс» основан в 1998 году и оснащен современным технологическим оборудованием, приборами для входного контроля исходных материалов, операционного контроля изготовления литейных форм, термической и механической обработки, приемки готовой продукции. Собственные производственные помещения располагаются в г. Краснодар, г. Челябинск, г. Могилев РБ. Доставка готовой продукции производится по всей России и республике Беларусь на собственном транспорте ООО НПО «Прогресс».\n\n<p class="technologies" style="box-sizing: border-box; margin: 25px 0px 10px; padding: 0px; font-family: RobotoSlab; color: rgb(60, 25, 1); font-size: 20.01px; font-weight: 700; transform: scaleY(1.001); text-transform: uppercase;">ТЕХНОЛОГИИ ПРОИЗВОДСТВА БАНЬ:</p>\n\n<ul class="technologies_ul padding_none" style="box-sizing: border-box; margin: 0px 0px 10px; list-style: none; font-family: Roboto; color: rgb(83, 70, 61); font-size: 17px; line-height: 35px; padding-right: 0px !important; padding-left: 25px !important;">\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Только экологические чистые материалы</li>\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Каждая баня проходит тщательную проверку на качество</li>\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Температура в бане всегда соответствует стандартам</li>\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Гарантирована противопожарная безопасность</li>\n</ul>\n', 'files/catalog/upload/08af358557ce938f6e6df5822c973ce7.png', 1),
(21, 'БАНЯ-БОЧКА «ДУО»', 25, NULL, 'ООО «НПО Прогресс» основан в 1998 году и оснащен современным технологическим оборудованием, приборами для входного контроля исходных материалов, операционного контроля изготовления литейных форм, термической и механической обработки, приемки готовой продукции. Собственные производственные помещения располагаются в г. Краснодар, г. Челябинск, г. Могилев РБ. Доставка готовой продукции производится по всей России и республике Беларусь на собственном транспорте ООО НПО «Прогресс».', 'НПО ПРОГРЕСС - БАНИ С ДОСТАВКОЙ\n\nООО «НПО Прогресс» основан в 1998 году и оснащен современным технологическим оборудованием, приборами для входного контроля исходных материалов, операционного контроля изготовления литейных форм, термической и механической обработки, приемки готовой продукции. Собственные производственные помещения располагаются в г. Краснодар, г. Челябинск, г. Могилев РБ. Доставка готовой продукции производится по всей России и республике Беларусь на собственном транспорте ООО НПО «Прогресс».\n\n<p class="technologies" style="box-sizing: border-box; margin: 25px 0px 10px; padding: 0px; font-family: RobotoSlab; color: rgb(60, 25, 1); font-size: 20.01px; font-weight: 700; transform: scaleY(1.001); text-transform: uppercase;">ТЕХНОЛОГИИ ПРОИЗВОДСТВА БАНЬ:</p>\n\n<ul class="technologies_ul padding_none" style="box-sizing: border-box; margin: 0px 0px 10px; list-style: none; font-family: Roboto; color: rgb(83, 70, 61); font-size: 17px; line-height: 35px; padding-right: 0px !important; padding-left: 25px !important;">\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Только экологические чистые материалы</li>\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Каждая баня проходит тщательную проверку на качество</li>\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Температура в бане всегда соответствует стандартам</li>\n	<li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style-image: url(&quot;/static/img/icons/Rectangle.svg&quot;);">Гарантирована противопожарная безопасность</li>\n</ul>\n', 'files/catalog/upload/3535b1c5612cbc59e7179aeb864c26f1.png', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `security_role`
--

CREATE TABLE `security_role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_role`
--

INSERT INTO `security_role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user`
--

CREATE TABLE `security_user` (
  `id` int(11) NOT NULL,
  `disabled` smallint(6) DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `security_user_role`
--

CREATE TABLE `security_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filters`
--
ALTER TABLE `filters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_action`
--
ALTER TABLE `forms_field_action`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_type`
--
ALTER TABLE `forms_field_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `infoblock`
--
ALTER TABLE `infoblock`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus_item`
--
ALTER TABLE `menus_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_bochka_type`
--
ALTER TABLE `position_bochka_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_characteristics`
--
ALTER TABLE `position_characteristics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_compan`
--
ALTER TABLE `position_compan`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_forms`
--
ALTER TABLE `position_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_gallery`
--
ALTER TABLE `position_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_news`
--
ALTER TABLE `position_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_reviews`
--
ALTER TABLE `position_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_setting`
--
ALTER TABLE `position_setting`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_slides`
--
ALTER TABLE `position_slides`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_videos`
--
ALTER TABLE `position_videos`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms`
--
ALTER TABLE `section_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms_settings`
--
ALTER TABLE `section_forms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_form_feedback`
--
ALTER TABLE `section_form_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_news`
--
ALTER TABLE `section_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_page`
--
ALTER TABLE `section_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_product`
--
ALTER TABLE `section_product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_role`
--
ALTER TABLE `security_role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_user`
--
ALTER TABLE `security_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`name`);

--
-- Индексы таблицы `security_user_role`
--
ALTER TABLE `security_user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `filters`
--
ALTER TABLE `filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `infoblock`
--
ALTER TABLE `infoblock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `menus_item`
--
ALTER TABLE `menus_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `position_bochka_type`
--
ALTER TABLE `position_bochka_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `position_characteristics`
--
ALTER TABLE `position_characteristics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `position_compan`
--
ALTER TABLE `position_compan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `position_forms`
--
ALTER TABLE `position_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `position_gallery`
--
ALTER TABLE `position_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `position_news`
--
ALTER TABLE `position_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `position_reviews`
--
ALTER TABLE `position_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `position_setting`
--
ALTER TABLE `position_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `position_slides`
--
ALTER TABLE `position_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `position_videos`
--
ALTER TABLE `position_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `section_product`
--
ALTER TABLE `section_product`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `security_user`
--
ALTER TABLE `security_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
